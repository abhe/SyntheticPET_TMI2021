# Synthetic PET via Domain Translation of 3D MRI

Code for the paper "Synthetic PET via Domain Translation of 3D MRI" by Rajagopal et. al., under review, 2021.

## Getting started

1. Clone this repo and associated subrepos
```
$ git clone --recurse-submodules -j8 git://gitlab.com/abhe/SyntheticPET_TMI2021
```

2. Take a look at an example HDF5 file (or bring your own with the same datatype):
```
$ ls -lh ./data/examples/exam-0000.hdf5
$ ipython -m "import h5py; hf = h5py.File('./data/examples/exam-0000.hdf5','r'); for k in hf.keys(): print(k, hf[k].dtype, hf[k].shape)
```

3. Generate synthetic PET via mr2pet domain translation:

4. Format/inspect your own quantification experiment for GE tools using Duetto:

5. Run a PET MRAC vs CTAC quantification experiment using synthetic PET (Note: running 1 exam takes 2+ hours in Duetto and requires Matlab):

6. Visualize the results:


## Figures from the paper








__________________________________________________________

To make it easy for you to get started with GitLab, here's a list of recommended next steps.

Already a pro? Just edit this README.md and make it your own. Want to make it easy? [Use the template at the bottom](#editing-this-readme)!

## Add your files

- [ ] [Create](https://gitlab.com/-/experiment/new_project_readme_content:b726795d46f7b4dfa7ad4e683e771c25?https://docs.gitlab.com/ee/user/project/repository/web_editor.html#create-a-file) or [upload](https://gitlab.com/-/experiment/new_project_readme_content:b726795d46f7b4dfa7ad4e683e771c25?https://docs.gitlab.com/ee/user/project/repository/web_editor.html#upload-a-file) files
- [ ] [Add files using the command line](https://gitlab.com/-/experiment/new_project_readme_content:b726795d46f7b4dfa7ad4e683e771c25?https://docs.gitlab.com/ee/gitlab-basics/add-file.html#add-a-file-using-the-command-line) or push an existing Git repository with the following command:

```
cd existing_repo
git remote add origin https://gitlab.com/abhe/SyntheticPET_TMI2021.git
git branch -M main
git push -uf origin main
```

## Integrate with your tools

- [ ] [Set up project integrations](https://gitlab.com/-/experiment/new_project_readme_content:b726795d46f7b4dfa7ad4e683e771c25?https://gitlab.com/abhe/SyntheticPET_TMI2021/-/settings/integrations)

## Collaborate with your team

- [ ] [Invite team members and collaborators](https://gitlab.com/-/experiment/new_project_readme_content:b726795d46f7b4dfa7ad4e683e771c25?https://docs.gitlab.com/ee/user/project/members/)
- [ ] [Create a new merge request](https://gitlab.com/-/experiment/new_project_readme_content:b726795d46f7b4dfa7ad4e683e771c25?https://docs.gitlab.com/ee/user/project/merge_requests/creating_merge_requests.html)
- [ ] [Automatically close issues from merge requests](https://gitlab.com/-/experiment/new_project_readme_content:b726795d46f7b4dfa7ad4e683e771c25?https://docs.gitlab.com/ee/user/project/issues/managing_issues.html#closing-issues-automatically)
- [ ] [Enable merge request approvals](https://gitlab.com/-/experiment/new_project_readme_content:b726795d46f7b4dfa7ad4e683e771c25?https://docs.gitlab.com/ee/user/project/merge_requests/approvals/)
- [ ] [Automatically merge when pipeline succeeds](https://gitlab.com/-/experiment/new_project_readme_content:b726795d46f7b4dfa7ad4e683e771c25?https://docs.gitlab.com/ee/user/project/merge_requests/merge_when_pipeline_succeeds.html)

## Test and Deploy

Use the built-in continuous integration in GitLab.

- [ ] [Get started with GitLab CI/CD](https://gitlab.com/-/experiment/new_project_readme_content:b726795d46f7b4dfa7ad4e683e771c25?https://docs.gitlab.com/ee/ci/quick_start/index.html)
- [ ] [Analyze your code for known vulnerabilities with Static Application Security Testing(SAST)](https://gitlab.com/-/experiment/new_project_readme_content:b726795d46f7b4dfa7ad4e683e771c25?https://docs.gitlab.com/ee/user/application_security/sast/)
- [ ] [Deploy to Kubernetes, Amazon EC2, or Amazon ECS using Auto Deploy](https://gitlab.com/-/experiment/new_project_readme_content:b726795d46f7b4dfa7ad4e683e771c25?https://docs.gitlab.com/ee/topics/autodevops/requirements.html)
- [ ] [Use pull-based deployments for improved Kubernetes management](https://gitlab.com/-/experiment/new_project_readme_content:b726795d46f7b4dfa7ad4e683e771c25?https://docs.gitlab.com/ee/user/clusters/agent/)
- [ ] [Set up protected environments](https://gitlab.com/-/experiment/new_project_readme_content:b726795d46f7b4dfa7ad4e683e771c25?https://docs.gitlab.com/ee/ci/environments/protected_environments.html)
